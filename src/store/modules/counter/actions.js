import { ADD, SUB } from "./actionTypes";

export const addOne = (currentNumber) => ({
  type: ADD,
  number: currentNumber,
});

export const subOne = (currentNumber) => ({
  type: SUB,
  number: currentNumber,
});
