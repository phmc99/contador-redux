import { ADD, SUB } from "./actionTypes";

const reducerCounter = (state = 0, action) => {
  const { number } = action;

  switch (action.type) {
    case ADD:
      return number + 1;

    case SUB:
      return number - 1;

    default:
      return state;
  }
};

export default reducerCounter;
