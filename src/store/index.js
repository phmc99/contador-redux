import { createStore, combineReducers } from "redux";

import reducer from "./modules/counter/reducer";

const reducers = combineReducers({ count: reducer });

const store = createStore(reducers);

export default store;
