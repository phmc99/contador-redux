import { useDispatch, useSelector } from "react-redux";
import "./App.css";
import { addOne, subOne } from "./store/modules/counter/actions";

function App() {
  const count = useSelector((state) => state.count);
  const dispatch = useDispatch();

  const handleAdd = () => dispatch(addOne(count));
  const handleSub = () => dispatch(subOne(count));

  return (
    <div className="App">
      <header className="App-header">
        <span>{count}</span>
        <button onClick={handleAdd}>adicionar</button>
        <button onClick={handleSub}>subtrair</button>
      </header>
    </div>
  );
}

export default App;
